! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Numeric seesaw
! https://edabit.com/challenge/sBCwuDxpC8wWQzSMA

program numeric_seesaw
  implicit none
  integer,dimension(:),allocatable :: list
  integer :: n
  
  print*,'====================================='
  print*,'Programming Practice - Numeric Seesaw'
  print*,'====================================='
  call init
  call seesaw
  
contains

  ! Read input
  subroutine init
    implicit none
    
    open(21, file="input")
    read(21,*) n
    allocate(list(n))
    read(21,*) list

    return
  end subroutine init

  ! Look for balance
  subroutine seesaw
    implicit none
    integer i,p,q,r
    integer,dimension(n) :: digit
    
    digit  = int(log10(real(list))+1)
    print*,list
    print*,digit
    
    do i = 1,n
       p = 0
       if (mod(digit(i),2).ne.0) p = 1
       q = list(i)/(10**((digit(i)+p)/2))
       r = mod(list(i),(10**((digit(i)-p)/2)))
       if (q.gt.r) print*,i,list(i),'Seesaw is left heavy!'
       if (q.lt.r) print*,i,list(i),'Seesaw is right heavy!'
       if (q.eq.r) print*,i,list(i),'Seesaw is balanced!'
    end do
    
    return
  end subroutine seesaw
  
end program numeric_seesaw
